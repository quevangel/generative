import std.stdio;
import std.math : PI;
import std.complex;
import derelict.sdl2.sdl;

SDL_Window* window; 
SDL_Renderer* renderer;
Complex!double[] polygonVertices;
Complex!double spaceSize = complex(500.0, 500.0);

void initPolygon()
{
    import std.random;
    int sides = 3;
    polygonVertices.length = sides;
    Complex!double center = spaceSize / 2.0;
    for(int i = 0; i < sides; i++)
    {
        polygonVertices[i] = center + (100 * uniform!"[]"(0.4, 1.7)) * expi(2.0 * PI * i / sides + uniform!"[]"(0.0, 0.5));
        version(none) polygonVertices[i] = center + (100 * uniform!"[]"(1.0, 1.0)) * expi(2.0 * PI * i / sides + uniform!"[]"(0.0, 0.0));
    }
}

bool insidePolygon(Complex!double c)
{
    int n = cast(int) polygonVertices.length;
    for(int i = 0; i < n; i++)
    {
        int ni = (i + 1) % n; 
        Complex!double currVertex = polygonVertices[i];
        Complex!double nextVertex = polygonVertices[ni];
        Complex!double segment = nextVertex - currVertex;
        Complex!double delta = c - currVertex;
        Complex!double transform = delta/segment;
        if (transform.im < 0)
            return false;
    }
    return true;
}

bool insidePolygonRay(Complex!double c)
{
    import std.random;
    int n = cast(int) polygonVertices.length;
    int count = 0;
    Complex!double ray = expi(uniform!"[)"(-PI, PI));
    for(int i = 0; i < n; i++)
    {
        int ni = (i + 1) % n;
        Complex!double da = (polygonVertices[i] - c) / ray;
        Complex!double db = (polygonVertices[ni] - c) / ray;
        // dai * k + dbi * (1 - k) = k * (dai - dbi) + dbi
        // k = -dbi / (dai - dbi)
        double k = - db.im / (da.im - db.im);
        if (k >= 0 && k <= 1)
        {
            double realPart = da.re * k + db.re * (1 - k);
            if (realPart >= 0)
                count++;
        }
    }
    return (count % 2) == 1;
}

void drawPolygonLines()
{
    int n = cast(int) polygonVertices.length;
    for(int i = 0; i < n; i++)
    {
        int ni = (i + 1) % n;
        auto curr = polygonVertices[i];
        auto next = polygonVertices[ni];
        SDL_RenderDrawLine(renderer, 
                cast(int) curr.re, cast(int) curr.im, cast(int) next.re, cast(int) next.im);
    }
}

double distanceToSegment(Complex!double c, Complex!double a, Complex!double b)
{
    static import std.math;
    import std.algorithm;
    Complex!double v = b - a;
    double mag = v.abs;
    Complex!double d = c - a;
    v /= abs(v);
    d /= v;
    double y = d.im;
    double x = d.re;
    if (x >= 0 && x <= mag)
        return std.math.abs(d.im);
    else
        return min(abs(d), abs(complex(mag) - d));
}

double distanceToLine(Complex!double c, Complex!double a, Complex!double b)
{
    static import std.math;
    import std.algorithm;
    Complex!double v = b - a;
    Complex!double d = c - a;
    v /= abs(v);
    d /= v;
    return std.math.abs(d.im);
}
void fillPolygon(double minDistance)
{
    import std.random;
    import std.algorithm;
    import std.math : pow;
    enum N = 500_000;
    for(int n = 0; n < N; n++)
    {
        int x = uniform!"[]"(0, 500);
        int y = uniform!"[]"(0, 500);
        Complex!double c = complex(x, y);
        if (insidePolygonRay(c))
        {
            int k = cast(int) polygonVertices.length;
            double closestDistance = distanceToSegment(c, polygonVertices[0], polygonVertices[1]);
            double closestVertexDistance = abs(c - polygonVertices[0]);
            for(int i = 0; i < k; i++)
            {
                Complex!double a = polygonVertices[i];
                Complex!double b = polygonVertices[(i + 1) % k];
                closestDistance = min(closestDistance, distanceToSegment(c, a, b));
                closestVertexDistance = min(closestVertexDistance, abs(c - polygonVertices[i]));
            }
            if (closestDistance <= minDistance)
            {
                double reverse = minDistance - closestDistance;
                reverse /= minDistance;
                reverse = pow(reverse, closestVertexDistance);
                if(uniform!"[]"(0.0, 1.0) <= reverse)
                {
                    SDL_Rect rect;
                    rect.x = x; 
                    rect.y = y;
                    rect.w = 1;
                    rect.h = 1;
                    SDL_RenderFillRect(renderer, &rect);
                }
            }
        }
    }
}

void fillPolygonWat(double minDistance)
{
    import std.random;
    import std.algorithm;
    import std.math : pow;
    enum N = 2_000_000;
    for(int n = 0; n < N; n++)
    {
        int x = uniform!"[]"(0, 500);
        int y = uniform!"[]"(0, 500);
        Complex!double c = complex(x, y);
        {
            int k = cast(int) polygonVertices.length;
            double closestDistance = distanceToLine(c, polygonVertices[0], polygonVertices[1]);
            double closestVertexDistance = abs(c - polygonVertices[0]);
            for(int i = 0; i < k; i++)
            {
                Complex!double a = polygonVertices[i];
                Complex!double b = polygonVertices[(i + 1) % k];
                closestDistance = min(closestDistance, distanceToLine(c, a, b));
                closestVertexDistance = min(closestVertexDistance, abs(c - polygonVertices[i]));
            }
            if (closestDistance <= minDistance)
            {
                double reverse = minDistance - closestDistance;
                reverse /= minDistance;
                reverse = pow(reverse, closestVertexDistance);
                if(uniform!"[]"(0.0, 1.0) <= reverse)
                {
                    SDL_Rect rect;
                    rect.x = x; 
                    rect.y = y;
                    rect.w = 1;
                    rect.h = 1;
                    SDL_RenderFillRect(renderer, &rect);
                }
            }
        }
    }
}
int main()
{
    initPolygon();
    DerelictSDL2.load();
    SDL_Init(SDL_INIT_EVERYTHING);
    window = SDL_CreateWindow("generative", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        500, 500, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(renderer, 250, 255, 200, 255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 50);
    fillPolygon(100.0);
    SDL_RenderPresent(renderer);
    SDL_Delay(10_000);

    return 0;
}
